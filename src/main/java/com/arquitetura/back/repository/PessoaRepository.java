package com.arquitetura.back.repository;

import com.arquitetura.back.entity.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

    @Query(value = "SELECT email FROM pessoa WHERE pessoa.email = :email ",
            nativeQuery=true
    )
    String exstingEmail(String email);

    @Query(value = "SELECT * FROM pessoa WHERE pessoa.email = :email ",
            nativeQuery=true
    )
    Pessoa findByEmail(String email);


}
